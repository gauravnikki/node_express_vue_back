module.exports = {
    port: 3000,
    db: {

        database:process.env.DB_NAME || 'shop',
        user: process.env.DB_USER || 'gaurav',
        password: process.env.DB_PASS || '123456',
        options: {

            dialect: process.env.DIALECT || 'sqlite',
            host: process.env.HOST || '127.0.0.1',
            storage: './shop.sqlite'


            
        }
    },
    authentication: {
        jwtSecret: process.env.JWT_SECRET || 'secret'

    }
}
