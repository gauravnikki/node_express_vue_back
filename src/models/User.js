const { Sequelize, BOOLEAN } = require('sequelize')


const sequelize = require('../util/database')

    const User = sequelize.define('user', {

        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            alloNull: false,
            primaryKey: true
        },
        email: {
            type: Sequelize.STRING,
            alloNull: false,
            primaryKey: true
            
        },
        password: {
            type: Sequelize.STRING,
            alloNull: false,

        },
        confirmPassword: {
            type: Sequelize.STRING,
            alloNull: false,

        },
        is_superuser:{
            type: BOOLEAN,
            allowNull: false
        },

    })


module.exports = User;



