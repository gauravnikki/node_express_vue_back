
const AuthController =  require('./controllers/AuthController')

const ProductController = require('./controllers/productController')

const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerpolicy')

module.exports =(app) => {

    app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthController.register
    
    )
    
    app.post('/login',AuthController.login)

    app.post('/addProduct',ProductController.postAddProduct)
    app.get('/getProducts',ProductController.getProducts)


}




