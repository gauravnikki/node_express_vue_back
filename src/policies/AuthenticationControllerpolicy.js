const  Joi = require('joi')



const register = (req,res,next) => {

    const schema = Joi.object ({

        email: Joi.string().email(),
        password: Joi.string().regex(
            new RegExp('^[a-zA-Z0-9]{6,32}$')
        ),
        confirmPassword: Joi.any().valid(Joi.ref('password')).required()
    })
    console.log(req.body)
    const {error, value} = schema.validate(req.body)

    if (error) {

        switch (error.details[0].context.key){

            case 'email':
                res.status(400).send({
                    error:' You must provide a valid email address',
                    iserror: true
                })
                break

            case 'password':

                res.status(400).send({
                    error:' You must provide a valid password',
                    iserror: true
                })
                break

            case 'confirmPassword':
            res.status(400).send({
                error:'Confirm password and password should match',
                iserror: true
            })
                break
            default:
              res.status(400).send({
                    error: 'invalid registration information',
                    iserror: true
            })
        } 
    }else {
        
        next() 
    }

 }

module.exports.register = register