const Product  =  require('../models/Product')


exports.postAddProduct = (req,res,next) => {

    const title =  req.body.postData.title
    const src = req.body.postData.src
    const price = req.body.postData.price
    const description = req.body.postData.description 
    const userId = req.body.postData.userId

    console.log(req.body.postData)

    console.log(description)

    Product.create({     

        title: title,
        src: src,
        price: price,
        description: description,
        userId: userId
    })
    .then(result => {
        res.send(result)
    })
    
    .catch(err => {

        console.log(err)
    })
    

}

exports.getProducts = (req,res,next) => {
    let setProducts = {
        title: '',
        src: '',
        price: 0,
        description: ''
      }
      let prod = []
    Product.findAll()
    .then(products =>{
      // console.log(userDoc[0]["password"])
      console.log("hererrrrrrrrrr")
      if(products.length === 0) {
        res.status(200).send({
          iserror:true,
          error:"No products to display"
        })
      }else{
        products.forEach(product => {
            setProducts.title = product.title
            setProducts.src = product.src
            setProducts.price = product.price
            setProducts.description = product.description
            prod.push(setProducts)
            setProducts = {}
        });
        res.status(200).send(prod)
      }
    })
    .catch(err => {
      console.log(err);
    })
   }