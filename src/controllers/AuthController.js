
const User  =  require('../models/User')
const jwt = require('jsonwebtoken')
const config =  require('../config/config')
const bcrypt = require('bcryptjs')
function jwtSignUser (user) {

  const ONE_WEEK = 60 * 60 
  return jwt.sign(user, config.authentication.jwtSecret ,{

    expiresIn: 60 * 60
  })
}

exports.register = (req, res, next) => {

  const email = req.body.email
  const password = req.body.password
  const confirmPassword = req.body.confirmPassword
  const saltRounds = 12
  User.findAll({ where:{email:email}})
  .then(userDoc => {
    if (userDoc.length !==0) {
      return res.send({
        error: "The Use already Exist",
        iserror:true 
      })
    }
    return bcrypt
      .hash(password, 12)
      .then(hashedPassword => {
        const user = new User({
          email: email,
          password: hashedPassword,
          confirmPassword: hashedPassword,
          is_superuser: false
        })
        return user.save()
      })
      .then(result => {
        res.status(200).send({iserror:false,result:result.toJSON()})
      })
  })
  .catch(err => {
    console.log(err);
  });
  
};

exports.login = (req,res,next) => {

  const email= req.body.email
  const password= req.body.password

  console.log(req.body)

  User.findAll({
    where:{email:email}
  })
  .then(userDoc =>{
    console.log(userDoc[0]["password"])
    // console.log(password)
    if(userDoc.length === 0) {
      res.status(200).send({
        iserror:true,
        error:"User does not exist"
      })
    }
    bcrypt
    .compare(password,userDoc[0]["password"])
    .then(doMatch => {
        if(doMatch) {
          const userJson = userDoc[0].toJSON()
          const response = res.status(200).send({
            msg:"login successfull",
            login:userJson,
            token: jwtSignUser(userJson)
          })
          return response
        }
        else {
          res.status(200).send({
            error: "User name or password are incorrect",
            iserror:true 
          })
        }
    })
    .catch(err => {
      console.log(err);
      res.status(200).send({
        error: err,
        iserror:true 
      })
    })
  })
 }
     // else if(!isPasswordValid){
    //   

    // }else{
    //   const userJson = userDoc[0].toJSON()
    //   console.log(userJson)
    //   res.status(200).send({
    //     msg:"login successfull",
    //     login:userJson,
    //     token: jwtSignUser(userJson)
    //   })
    // }
//   })
//   .catch(err => {
// 