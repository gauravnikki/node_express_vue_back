
const express = require ('express')
const bodyParser = require('body-parser')
const cors = require("cors")
const morgan = require("morgan")

const sequelize  = require('./src/util/database')

// const config = require('./src/config/config')
const Product =  require("./src/models/Product")    
const User =  require("./src/models/User") 

const app =  express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())
require('./src/routes')(app)

sequelize.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch((error) => {
        console.error('Unable to connect to the database: ', error);
    });

Product.belongsTo(User, {constraints: true, onDelete: 'CASCADE'})

User.hasMany(Product)

sequelize
    // .sync({force:true})
    .sync()
    .then((result) => {
        // console.log(result)
        app.listen(3000)

        console.log(`Server started on port 3000`)
    })
    .catch(err => {
        console.log(err)
    })

